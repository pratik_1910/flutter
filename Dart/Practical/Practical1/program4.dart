import "dart:io";

void main(){
 
 int space=0;
 int row=3;
 int col=0;

 for(int i=1;i<=(row*2)-1;i++){
  if(row>=i){
    space=row-i;
  }else{
    space=i-row;
  }
  for(int sp=1;sp<=space;sp++){
    stdout.write("   ");
  }

  if(row>=i){
    col=i;
  }else{
    col=col-1;
  }
  for(int j=1;j<=col;j++){
    stdout.write(" * ");
  }
  stdout.writeln();
 }
  
}