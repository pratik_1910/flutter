import "dart:io";

void main(){
  int row=6;
  int col=0;
  int n=1;
  for(int i=1;i<=2*row;i++){
    if(i<=row){
      col=(2*i)-1;
    }else{
      col=(2*i)-(2*n)-1;
      n=n+2;
    }

    for(int j=1;j<=col;j++){
        stdout.write(" * ");
    }
    stdout.writeln();
  }
}