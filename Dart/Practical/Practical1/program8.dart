import "dart:io";

void main(){
  int row=10;
  int n=1;
  for(int i=1;i<=9;i++){
    if(i<=(row/2)){
      for(int j=1;j<=10;j++){
        if(j>10-(2*i)){
          stdout.write(" * ");
        }else{
          stdout.write("   ");
        }
      }
    }else{
      for(int j=1;j<=10;j++){
        if(j>(2*n)){
          stdout.write(" * ");
        }else{
          stdout.write("   ");
        }
      }
      n++;
    }
    stdout.writeln();
  }
}